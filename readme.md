# CoursePlus
[User](http://demo.foxthemes.net/courseplus/default/index.html) Learning [CoursePlus](http://demo.foxthemes.net/courseplus/) web html template.

## Screen shots
![](screen_shots/dashboard.png)

![](screen_shots/course-path.png)

![](screen_shots/course-path-levels.png)

![](screen_shots/course-grid.png)

![](screen_shots/courses-list.png)

![](screen_shots/course-introduction.png)

![](screen_shots/course-resume.png)

![](screen_shots/episodes.png)

![](screen_shots/book.png)

![](screen_shots/book-description.png)

![](screen_shots/blog-1.png)

![](screen_shots/blog-2.png)

![](screen_shots/blog-card.png)

![](screen_shots/blog-single-1.png)

![](screen_shots/blog-single-2.png)

![](screen_shots/profile.png)

![](screen_shots/profile-edit.png)

![](screen_shots/page-pricing.png)

![](screen_shots/privecy.png)

![](screen_shots/page-term.png)

![](screen_shots/page-faq.png)

![](screen_shots/page-comming-soon.png)

![](screen_shots/form-modurn-login.png)